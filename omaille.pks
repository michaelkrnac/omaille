/* omaille.pks
 *
 * Description: Defines the structure of the package omaille
 *
 * Author: Michael Krnac (michael@krnac.de)
 *
 * Copyright (C) 2016 Michael Krnac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

CREATE OR REPLACE PACKAGE omaille AS
   PROCEDURE create_message (
        sender VARCHAR2,
        recipients VARCHAR2,
        cc VARCHAR2 DEFAULT NULL,
        bcc VARCHAR2 DEFAULT NULL,
        subject VARCHAR2 DEFAULT NULL,
        message VARCHAR2 DEFAULT NULL,
        mime_type VARCHAR2 DEFAULT 'text/plain; charset=UTF-8',
        priority VARCHAR2 DEFAULT 3
    );
   PROCEDURE robo_send;
   PROCEDURE create_job_robo_send;
END omaille;
