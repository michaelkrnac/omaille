/* omaille.pkb
 *
 * Description: Defines the body of the package omaille
 * 
 * Author: Michael Krnac (michael@krnac.de)
 *
 * Copyright (C) 2016 Michael Krnac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

CREATE OR REPLACE PACKAGE BODY omaille AS

    /*
     * create_message
     * create a new message in the message_queue
    */
    PROCEDURE create_message (
        sender VARCHAR2,
        recipients VARCHAR2,
        cc VARCHAR2 DEFAULT NULL,
        bcc VARCHAR2 DEFAULT NULL,
        subject VARCHAR2 DEFAULT NULL,
        message VARCHAR2 DEFAULT NULL,
        mime_type VARCHAR2 DEFAULT 'text/plain; charset=UTF-8',
        priority VARCHAR2 DEFAULT 3
    ) IS
    BEGIN
        INSERT INTO omaille_message_queue (
            sender,
            recipients,
            cc,
            bcc,
            subject,
            message,
            mime_type,
            priority)
        VALUES (
            sender,
            recipients,
            cc,
            bcc,
            subject,
            message,
            mime_type,
            priority);
        COMMIT;
    END create_message;   

    /*
     * robo_send
     * loop over the message queue and send each
     * new message using UTL_MAIL
    */
    PROCEDURE robo_send IS
   	     CURSOR mails IS
            SELECT *
		    FROM omaille_message_queue
		    WHERE status = 'new';
    BEGIN
        FOR mail IN mails LOOP
		    BEGIN
			    UTL_MAIL.SEND(
				    sender => mail.sender,
			    	recipients => mail.recipients,
			    	cc => mail.cc,
			    	bcc => mail.bcc,
			    	subject => mail.subject,
			    	message => mail.message,
			    	mime_type => mail.mime_type,
			    	priority => mail.priority
		    	);	

		    	UPDATE omaille_message_queue
		    	SET status = 'sent',
                    send_date = SYSTIMESTAMP
		    	WHERE message_id = mail.message_id;
		        COMMIT;
		    EXCEPTION WHEN OTHERS THEN
		
		    	UPDATE omaille_message_queue
			    SET status = 'failed',
                    send_date = NULL
			    WHERE message_id = mail.message_id;
		        COMMIT;
		    END;
	    END LOOP;
    END robo_send;

    /*
     * create_job_robo_send
     * create a scheduled job to send out
     * all new messages in the message queue
    */
    PROCEDURE create_job_robo_send IS
    BEGIN
        DBMS_SCHEDULER.CREATE_JOB (
            job_name           => 'omaille_robo_send',
            job_type           => 'STORED_PROCEDURE',
            job_action         => 'omaille.robo_send',
            repeat_interval    => 'FREQ=MINUTELY;INTERVAL=5', /* every 5 minutes */
            comments           => 'Run robo_send to send all new messages in the message_queue');    
    END create_job_robo_send;
END omaille;
