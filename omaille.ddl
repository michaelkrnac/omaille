/* omaille_message_queue.ddl
 *
 * Description: Defines the structure of the table 
 * where all emails that should be send are stored.
 *
 * Author: Michael Krnac (michael@krnac.de)
 *
 * Copyright (C) 2016 Michael Krnac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

CREATE TABLE omaille_message_queue (
       message_id   NUMBER(12,0) NOT NULL,
       sender 	    VARCHAR2 (2000 CHAR) NOT NULL,
       recipients   VARCHAR2 (2000 CHAR) NOT NULL,
       cc 	        VARCHAR2 (2000 CHAR) DEFAULT NULL,
       bcc 	        VARCHAR2 (2000 CHAR) DEFAULT NULL,
       subject 	    VARCHAR2 (2000 CHAR) DEFAULT NULL,
       message 	    VARCHAR2 (4000 CHAR) DEFAULT NULL,
       mime_type    VARCHAR2 (1000 CHAR) DEFAULT 'text/plain; charset=UTF-8',
       priority     NUMBER(1) DEFAULT 3,
       status 	    VARCHAR2 (100 CHAR) DEFAULT 'new',
       create_date  TIMESTAMP DEFAULT SYSTIMESTAMP,
       send_date    TIMESTAMP DEFAULT NULL
);

COMMENT ON TABLE  omaille_message_queue IS
	'A queue of emails that should be send by the sendrobot' ;

COMMENT ON COLUMN omaille_message_queue.message_id IS
	'ID for each message that leaves the server.' ;

COMMENT ON COLUMN omaille_message_queue.sender IS
	'The email address of the sender.';
	
COMMENT ON COLUMN omaille_message_queue.recipients IS
	'The email addresses of the recipient(s), separated by commas.';

COMMENT ON COLUMN omaille_message_queue.cc IS
	'The email addresses of the CC recipient(s), separated by commas.';

COMMENT ON COLUMN omaille_message_queue.bcc IS
	'The email addresses of the BCC recipient(s), separated by commas.';

COMMENT ON COLUMN omaille_message_queue.subject	IS
	'Subject of the Email';

COMMENT ON COLUMN omaille_message_queue.message IS
	'A text message body.';

COMMENT ON COLUMN omaille_message_queue.mime_type IS
	'The mime type of the message, default is text/plain; charset=UTF-8';

COMMENT ON COLUMN omaille_message_queue.priority IS
	'Message priority, which maps to the X-priority field. 1 is the highest priority and 5 the lowest. The default is 3.';

COMMENT ON COLUMN omaille_message_queue.status IS
	'Status of the email: new,sent,failed';

COMMENT ON COLUMN omaille_message_queue.create_date IS
	'Timestamp when the message was added to the queue';

COMMENT ON COLUMN omaille_message_queue.send_date IS
	'Timestamp when the message was send by the robot';
    


ALTER TABLE  omaille_message_queue
ADD CONSTRAINT omaille_message_queue_pk PRIMARY KEY ( message_id ) ;

CREATE SEQUENCE sqn_omaille_message_queue START WITH 1 NOCACHE ORDER;

CREATE OR REPLACE TRIGGER omaille_message_queue_insert
       BEFORE INSERT ON omaille_message_queue
       FOR EACH ROW
       WHEN (NEW.message_id IS NULL)
       	    BEGIN :NEW.message_id := sqn_omaille_message_queue.NEXTVAL;
END;
