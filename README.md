# Omaille
Omaille is an ORACLE Package using UTL_MAIL to send emails from a message queue

# Installation
 - run omaille.ddl
 - compile omaille.pks and omaille.pkb
 - create job to send messages
 
    ```sql
      -- create job to send messages    
      BEGIN
          omaille.create_job_robo_send;
      END;
    ```
 - enable job
 
# Usage
  - Add new emails to the message queue

```sql
-- create message text
BEGIN
    omaille.create_message(
        sender      => 'michael@krnac.de',
        recipients  => 'michael@krnac.de'
    );
END;

-- create message text 
BEGIN
    omaille.create_message(
        sender      => 'michael@krnac.de',
        recipients  => 'michael@krnac.de',
        cc          => 'michael@krnac.de',
        bcc         => 'michael@krnac.de',
        subject     => 'TEST: create message text maxi',
        message     => 'MESSAGE TEST: create message text maxi'
    );
END;

-- create message html
BEGIN
    omaille.create_message(
        sender      => 'michael@krnac.de',
        recipients  => 'michael@krnac.de',
        cc          => 'michael@krnac.de',
        bcc         => 'michael@krnac.de',
        subject     => 'TEST: create message html maxi',
        message     => '<h1>MESSAGE TEST:</h1> create message text maxi <a href="http://www.google.de">GOOGLE</a>',
        mime_type   => 'text/html; charset=UTF-8'
    );
END;
```

  - wait until the job sends the emails (default every 5 minutes)
