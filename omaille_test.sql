/* omaille_test.sql
 *
 * Description: Test Data for package omaille
 * 
 * Author: Michael Krnac (michael@krnac.de)
 *
 * Copyright (C) 2016 Michael Krnac
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

-- create message text mini
BEGIN
    omaille.create_message(
        sender      => 'michael@krnac.de',
        recipients  => 'michael@krnac.de'
    );
END;

-- create message text maxi
BEGIN
    omaille.create_message(
        sender      => 'michael@krnac.de',
        recipients  => 'michael@krnac.de',
        cc          => 'michael@krnac.de',
        bcc         => 'michael@krnac.de',
        subject     => 'TEST: create message text maxi',
        message     => 'MESSAGE TEST: create message text maxi'
    );
END;

-- create message html maxi
BEGIN
    omaille.create_message(
        sender      => 'michael@krnac.de',
        recipients  => 'michael@krnac.de',
        cc          => 'michael@krnac.de',
        bcc         => 'michael@krnac.de',
        subject     => 'TEST: create message html maxi',
        message     => '<h1>MESSAGE TEST:</h1> create message text maxi <a href="http://www.google.de">GOOGLE</a>',
        mime_type   => 'text/html; charset=UTF-8'
    );
END;

-- send messages manual   
BEGIN
    omaille.robo_send;
END;

-- create job to send messages    
BEGIN
    omaille.create_job_robo_send;
END;
